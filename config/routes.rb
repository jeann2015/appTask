Rails.application.routes.draw do

  # root 'static_pages#home'
  get  '/help',    to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'
  get  '/signup',  to: 'users#create'
  get  '/login',  to: 'users#new'
  post  '/validate',  to: 'users#validate'
  post  '/users/save',  to: 'users#save'
  resources :users



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
