class UsersController < ApplicationController

  def new
  end

  def index
  end

  def show
  end

  def root
  end

  def validate
    @user = User.where(user_params_email_password).first

    if @user
      render 'root'
    else
      flash[:success] = "User No Found!"
      render 'new'
    end
  end

  def save
    @user = User.new(user_params)
    if @user.save
      flash[:success] = "Welcome to the Sample App!"
      render 'new'
    end
  end

  def user_params
    params.require(:user).permit(:name, :email, :password, :typeu)
  end

  def user_params_email_password
    params.require(:user).permit(:email, :password)
  end

end
